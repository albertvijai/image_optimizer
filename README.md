#IMAGE OPTIMIZATION STANDARD POC#
GOAL: Create a set of standards for Websites to load and renter images fast:

##PRE PROCESS IMAGES##
Before loading the image to the site, Compress the image if possible.	
http://enviragallery.com/9-best-free-image-optimization-tools-for-image-compression/	
-Set a standard on image dimentions and image size for this optimization


##SERVER SIDE##
- Enable Blob Cache: web.config	
<BlobCache location="<LOCATION TO CACHE ON IIS>" path="\.(gif|jpg|jpeg|jpe|jfif|bmp|dib|tif|tiff|themedbmp|themedcss|themedgif|themedjpg|themedpng|ico|png|wdp|hdp|css|js|asf|avi|flv|m4v|mov|mp3|mp4|mpeg|mpg|rm|rmvb|wma|wmv|ogg|ogv|oga|webm|xap)$" maxSize="10" enabled="false" />
- Configure Image Rendition: Go to Settings -> Site Settings -> Look and Fee -> Image Rendition	
Use this option to configure large images. 

##CLIENT SIDE##
If the page has a lot of images, the client can lazy load the images.	
This allows for the page to load with a placeholder image on Init and then lazy load all the image assets asyncronously.

There are a couple of vanilla solutions: The below POC attemps to show both in the following order.	
<<<<<<< HEAD
- jquery_lazyload:WIP
- Yahoo UI Image Loader:STABLE
=======
- jquery_lazyload	:WIP

- Yahoo UI Image Loader	:STABLE
Example: https://yuilibrary.com/yui/docs/imageloader/basic-features.html
>> DELAYED LOADING
>> EVENT BASED LOADING (SCROLL, CLICK, MOUSE OVER)
>> GROUPED LOADING:

##CDN## (Optional Consideration: Might be slower based on implementation)
Use a CDN if possible. Static assets can be centralized to a content delivery network.
This can help to loosely couple static assets and server pages that access them.
This can also help reduce duplication of images.
>>>>>>> POC WIP

